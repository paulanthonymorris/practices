﻿/// Rudimentary proof of concept for using ReactiveX for our messaging in Java and .NET
using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace MessagingReactiveX
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER to exit...\n");

            var broker = new MessageBroker();

            // The subscriptions are IDisposables so we bubble that pattern up to the containing class.
            using (new TopLevelConsumer(broker))
            {
                Console.ReadLine();
            }
        }
    }

    /// <summary>
    /// This is a simplistic hierarchy system. The higher the enum value the greater 
    /// the specificity (see <a>https://www.youtube.com/watch?v=t2lzVy7lkVM</a> 0:36). 
    /// Not sure this would be enough but it gives you an idea of what I was thinking 
    /// of to address the problem of addressing.
    /// </summary>
    enum MessageType
    {
        General,     
        Specific,

        // No one will subscribe to this one in our example. This no-op message is  
        // the reason you will see periods of no activity in the console.
        VerySpecific
    }

    interface IMessage
    {
        int Index { get; }

        MessageType MessageType { get; }
    }

    class Message : IMessage
    {
        public Message(int index, MessageType messageType)
        {
            Index = index;
            MessageType = messageType;
        }

        public int Index { get; private set; }

        public MessageType MessageType { get; private set; }
    }

    class MessageBroker
    {
        /// <summary>
        /// Only goes hot after its first subscription.
        /// </summary>
        public IObservable<IMessage> ColdSource { get; private set; }

        /// <summary>
        /// Helps us solve the problem of the late subscriber. Only goes hot once we 
        /// manually connect it.
        /// </summary>
        public IConnectableObservable<IMessage> ConnectableSource { get; private set; }

        public MessageBroker()
        {
            ColdSource = Observable
                .Generate(
                    1,           // Initial state value
                    x => true,   // The while condition.
                    x => x + 1,  // Iteration step function updates the state and returns the new state. In this case increments by 1.
                    x => 
                    {
                        // Randomly alternate between the 3 message types
                        var random = new Random().Next(0, 3);
                        IMessage msg;
                        switch (random)
                        {
                            case 0:
                                msg = new Message(x, MessageType.General);
                                break;
                            case 1:
                                msg = new Message(x, MessageType.Specific);
                                break;
                            case 2: default:
                                msg = new Message(x, MessageType.VerySpecific);
                                break;
                        }

                        return msg;
                    },
                    x => TimeSpan.FromSeconds(5)
                );

            // IConnectableObservable could help us solve the problem of the late subscriber.
            ConnectableSource = Observable
                .Return(new Message(0, MessageType.General))
                .Publish();
        }
    }

    class TopLevelConsumer : IDisposable
    {
        private readonly IDisposable _coldSourceSubscription;
        private readonly IDisposable _connectableSourceSubscription;
        private readonly NestedConsumer _nestedConsumer;

        public TopLevelConsumer(MessageBroker broker)
        {
            _nestedConsumer = new NestedConsumer(broker);

            _coldSourceSubscription = broker.ColdSource
                // So we filter BEFORE OnNext is called. In RxJava the Where would be a call to 
                // filter(x => true|false) <a>http://reactivex.io/documentation/operators/filter.html</a>
                .Where(msg => msg.MessageType == MessageType.General)
                .Subscribe(msg => 
                    Console.WriteLine(
                        string.Format("Top-level consumer received message {0} of type {1}...", msg.Index, msg.MessageType)));

            // So the race condition above is that the nested consumer will register  
            // its subscription before the top-level consumer which means the observable 
            // will go hot at that point. If the first message happens to be a General 
            // message then the nested consumer will call OnNext (taking that message), 
            // and by the time the top-level consumer subscribes, that message is gone 
            // forever. This is a manifestation of the presence issue that Christopher 
            // referred to in his write-up. Using an IConnectableObservable allows us to 
            // defer publishing messages until all subscribers are ready.

            // RxJava also has this same structure <a>http://reactivex.io/documentation/operators.html#connectable</a>

            _connectableSourceSubscription = broker.ConnectableSource
                .Subscribe(msg =>
                    Console.WriteLine(
                        string.Format("Top-level consumer received message {0} of type {1} from a connectable source...", 
                            msg.Index, msg.MessageType)));

            // Ok. Both consumers are wired up. Let's let the source know he can start to produce 
            // messages now.
            broker.ConnectableSource.Connect();
        }

        public void Dispose()
        {
            _nestedConsumer.Dispose();
            _coldSourceSubscription.Dispose();
            _connectableSourceSubscription.Dispose();
        }
    }

    class NestedConsumer : IDisposable
    {
        private readonly IDisposable _coldSourceSubscription;
        private readonly IDisposable _connectableSourceSubscription;

        public NestedConsumer(MessageBroker broker)
        {
            _coldSourceSubscription = broker.ColdSource 
                // So here we are saying <= 1, that is, Specific or General
                .Where(msg => msg.MessageType <= MessageType.Specific)
                .Subscribe(msg => 
                    Console.WriteLine(
                        string.Format("Nested consumer received message {0} of type {1}...", msg.Index, msg.MessageType)));

            _connectableSourceSubscription = broker.ConnectableSource
                .Subscribe(msg =>
                    Console.WriteLine(
                        string.Format("Nested consumer received message {0} of type {1} from a connectable source...",
                            msg.Index, msg.MessageType)));
        }

        public void Dispose()
        {
            _coldSourceSubscription.Dispose();
            _connectableSourceSubscription.Dispose();
        }
    }
}
