﻿// Proof of concept
using Splat;
using SQLite.Net;
using SQLite.Net.Interop;

namespace Infrastructure.SQLite
{
    /// <summary>
    /// Wrapper class lets us reimplement Dispose(bool) so that we can retain the current 
    /// pattern of using connections inside using blocks but have our SQLiteDatabase act 
    /// as a middleman to decide whether to actually dispose of the underlying connection 
    /// or cache the connection.<para/>
    /// We don't follow our usual pattern of creating an interface to program against 
    /// (ISQLiteCacheableConnection) for the following reasons:
    /// <para>1. The PCL library does not have an ISQLiteConnection interface to extend.
    /// </para>
    /// <para>2. The helpful extension methods from the SQLiteNetExtensions library are 
    /// written against the concrete SQLiteConnection class</para>
    /// </summary>
    internal class SQLiteCacheableConnection : SQLiteConnection, IEnableLogger
    {
        #region Private members

        private const SQLiteOpenFlags _readOnlyOpenFlags = 
            SQLiteOpenFlags.ReadOnly | SQLiteOpenFlags.SharedCache;

        private const SQLiteOpenFlags _readWriteOpenFlags = 
            SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.SharedCache;

        private readonly ISQLiteDatabase _database;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates the connection against the passed in database.
        /// </summary>
        /// <param name="database"></param>
        internal SQLiteCacheableConnection(ISQLiteDatabase database, bool readOnly) 
            : base(database.Platform, database.Path, readOnly ? _readOnlyOpenFlags : _readWriteOpenFlags)
        {
            _database = database;
        }

        #endregion

        #region Public methods

        #region IDisposable implementation

        /// <summary>
        /// Reimplement Dispose(bool) so that the parent database can decide whether to cache 
        /// the connection before calling the base SQLiteConnection Dispose(bool) implementation
        /// which actually closes the file handle.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (!_database.TryCacheConnection(this))
            {
                this.Log().Debug("Database refused to cache connection {0}. Closing.", this);
                base.Dispose(disposing);
            }
        }

        #endregion

        #endregion
    }
}
