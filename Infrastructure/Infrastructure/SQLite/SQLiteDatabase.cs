﻿// Proof of concept
using Splat;
using SQLite.Net;
using SQLite.Net.Interop;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Infrastructure.SQLite
{
    /// <summary>
    /// See interface comments.
    /// </summary>
    /// <see cref="ISQLiteDatabase"/>
    internal class SQLiteDatabase : ISQLiteDatabase, IEnableLogger
    {
        #region Private members

        /// <summary>
        /// Answers how many read-only will be retained in the cache but does not limit how 
        /// many can be open at any one time.
        /// </summary>
        private const int MaxRetainedReadOnlyConnections = 5;

        /// <summary>
        /// The underlying object pool for connections.
        /// </summary>
        private readonly ConcurrentBag<SQLiteConnection> _readOnlyConnectionCache;

        /// <summary>
        /// The single read-write connection lazy loaded.
        /// </summary>
        private readonly Lazy<SQLiteConnection> _readWriteConnection;

        /// <summary>
        /// There may be a call for this database to be closed while a connection is in
        /// use. We would not want to close that connection arbitrarily but as soon as
        /// it is released we will call Close on it.
        /// </summary>
        private bool _closing;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a database wrapper object for the SQLite database file at the given path.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="databasePath"></param>
        internal SQLiteDatabase(ISQLitePlatform platform, string databasePath)
        {
            Platform = platform;
            Path = databasePath;
            _readOnlyConnectionCache = new ConcurrentBag<SQLiteConnection>();
            _readWriteConnection = new Lazy<SQLiteConnection>(
                () => 
                {
                    var readWriteConn = new SQLiteCacheableConnection(this, readOnly: false);
                    readWriteConn.ExecuteScalar<string>("PRAGMA journal_mode=wal;");

                    return readWriteConn;
                });
        }

        #endregion

        #region Public properties

        /// <summary>
        /// See interface comments for this property.
        /// </summary>
        /// <see cref="ISQLiteDatabase"/>
        public string Path { get; private set; }

        /// <summary>
        /// See interface comments for this property.
        /// </summary>
        /// <see cref="ISQLiteDatabase"/>
        public ISQLitePlatform Platform { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        /// See interface comments for this method.
        /// </summary>
        /// <see cref="ISQLiteDatabase"/>
        public SQLiteConnection GetReadOnlyConnection()
        {
            SQLiteConnection connection;
            if (!_readOnlyConnectionCache.TryTake(out connection))
            {
                connection = new SQLiteCacheableConnection(this, readOnly: true);
            }

            return connection;
        }

        /// <summary>
        /// See interface comments for this method.
        /// </summary>
        /// <see cref="ISQLiteDatabase"/>
        public SQLiteConnection GetReadWriteConnection()
        {
            Monitor.Enter(_readWriteConnection);

            return _readWriteConnection.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <see cref="ISQLiteDatabase"/>
        public bool TryCacheConnection(SQLiteConnection connection)
        {
            if (connection == _readWriteConnection.Value)
            {
                Monitor.Exit(_readWriteConnection);

                return true;
            }

            if (_readOnlyConnectionCache.Count < MaxRetainedReadOnlyConnections && !_closing)
            {
                _readOnlyConnectionCache.Add(connection);
                this.Log().Debug("Stashed an RO conn in the cache. Cache count is now {0}", 
                    _readOnlyConnectionCache.Count);

                return true;
            }

            return false;
        }

        #endregion

        #region Internal methods

        /// <summary>
        /// Not something to be exposed on the public API but a useful internal handle for  
        /// the SQLiteAccessService to clean up all connections when releasing a database.
        /// </summary>
        internal void CloseAllConnections()
        {
            _closing = true;

            this.Log().Debug("Closing connections on {0}", System.IO.Path.GetFileName(Path));

            SQLiteConnection connection;
            while (_readOnlyConnectionCache.TryTake(out connection))
            {
                connection.Close();
            }

            Monitor.Enter(_readWriteConnection);

            _readWriteConnection.Value.Close();

            Monitor.Exit(_readWriteConnection);
        }

        #endregion
    }
}
