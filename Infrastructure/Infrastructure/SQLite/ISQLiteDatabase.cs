﻿// Proof of concept
using SQLite.Net;
using SQLite.Net.Interop;

namespace Infrastructure.SQLite
{
    /// <summary>
    /// Represents a database and adds support for connection caching.
    /// </summary>
    public interface ISQLiteDatabase
    {
        #region Properties

        /// <summary>
        /// The file path.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// The specific platform passed into the PCL.
        /// </summary>
        ISQLitePlatform Platform { get; }

        #endregion

        #region Methods
        
        /// <summary>
        /// Retrieves a read-only cached connection to this database opening a new one if necessary. 
        /// Normally used in a using block.
        /// <para/>
        /// using (var connection = db.GetReadOnlyConnection()) { ... }
        /// </summary>
        SQLiteConnection GetReadOnlyConnection();

        /// <summary>
        /// Retrieves the read-write cached connection to this database opening a new one if necessary. 
        /// Normally used in a using block. There will never be more than one read-write connection 
        /// per database so concurrent callers will have wait for the connection to be released before 
        /// this call will return.
        /// <para/>
        /// using (var connection = db.GetReadWriteConnection()) { ... }
        /// </summary>
        SQLiteConnection GetReadWriteConnection();

        /// <summary>
        /// On Dispose, the ISQLiteDatabase decides whether to cache this connection or not. If the 
        /// ISQLiteDatabase doesn't want to cache this connection (which would only be the case on 
        /// a read-only connection when the MaxRetainedConnections had been exceeded), we call Dispose 
        /// on the underlying SQLiteConnection. Normally this method would not be called explicitly 
        /// but it would be called by the the Dispose() method of our SQLiteConnection wrapper at the 
        /// end of a using block, like so:
        /// <para/>
        /// using (var connection = db.GetReadXXXConnection()) { }
        /// </summary>
        /// <param name="connection"></param>
        bool TryCacheConnection(SQLiteConnection connection);

        #endregion
    }
}
