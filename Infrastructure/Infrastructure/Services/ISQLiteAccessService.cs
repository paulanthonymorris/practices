﻿// Proof of concept
using SQLite.Net.Interop;
using Infrastructure.SQLite;

namespace Infrastructure.Services
{
    /// <summary>
    /// The top-level service that serves as an entry point for direct access to SQLite 
    /// databases. Designed to be a singleton.
    /// </summary>
    public interface ISQLiteAccessService
    {
        #region Methods

        /// <summary>
        /// Closes all databases which means closing their underlying connections. 
        /// Might be used when suspending the app, etc.
        /// </summary>
        void CloseAllDatabases();

        /// <summary>
        /// Retrieves a cached database object creating a new one if necessary.
        /// </summary>
        /// <param name="databasePath"></param>
        /// <returns>
        /// An object representing a specific database with support for cached connections.
        /// </returns>
        ISQLiteDatabase GetDatabase(string databasePath);

        #endregion

        #region Properties

        /// <summary>
        /// The specific platform passed into the PCL.
        /// </summary>
        ISQLitePlatform Platform { get; }

        #endregion
    }
}
