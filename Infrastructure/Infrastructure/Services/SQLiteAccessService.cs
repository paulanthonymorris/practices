﻿// Proof of concept
using Splat;
using SQLite.Net.Interop;
using Infrastructure.SQLite;

namespace Infrastructure.Services
{
    /// <summary>
    /// See interface comments.
    /// </summary>
    /// <see cref="ISQLiteAccessService"/>
    public class SQLiteAccessService : ISQLiteAccessService
    {
        #region Private members

        private const int DatabaseCacheSize = 20;

        /// <summary>
        /// Cached databases. We must only ever have a single instance of each database in  
        /// order for connection caching to function correctly.
        /// </summary>
        /// <remarks>
        /// In the case of this class MRU means that it keeps the most recently used and 
        /// discards the least recently used. 
        /// http://blog.paulbetts.org/index.php/2010/07/13/reactivexaml-series-using-memoizingmrucache/
        /// </remarks>
        private readonly MemoizingMRUCache<string, SQLiteDatabase> _databaseCache;

        #endregion

        #region Ctor

        /// <summary>
        /// Set the platform and init the cache in a functional style.
        /// </summary>
        /// <param name="platform"></param>
        public SQLiteAccessService(ISQLitePlatform platform = null)
        {
            Platform = platform ?? Locator.Current.GetService<ISQLitePlatform>();
            Platform.SQLiteApi.Config(ConfigOption.Serialized);

            _databaseCache = new MemoizingMRUCache<string, SQLiteDatabase>(
                // ctx is an optional parameter that we don't need in this case.
                calculationFunc: (path, ctx) => new SQLiteDatabase(Platform, path),               
                maxSize: DatabaseCacheSize,
                onRelease: db => db.CloseAllConnections());
        }

        #endregion

        #region Public properties

        /// <summary>
        /// See interface comments.
        /// </summary>
        /// <see cref="ISQLiteAccessService"/>
        public ISQLitePlatform Platform { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        /// See interface comments.
        /// </summary>
        /// <see cref="ISQLiteAccessService"/>
        public void CloseAllDatabases()
        {
            // InvalidateAll not only empties the cache but fires the onRelease   
            // on each entry, in this case, CloseAllConnections().
            _databaseCache.InvalidateAll();
        }

        /// <summary>
        /// See interface comments.
        /// </summary>
        /// <see cref="ISQLiteAccessService"/>
        public ISQLiteDatabase GetDatabase(string databasePath)
        {
            lock (_databaseCache)
            {
                return _databaseCache.Get(databasePath);
            }
        }

        #endregion
    }
}
