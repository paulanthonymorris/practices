﻿// Proof of concept
using Infrastructure.Services;
using Splat;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using SQLite.Net.Platform.WinRT;
using SQLiteNetExtensions.Attributes;
using SQLiteNetExtensions.Extensions;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;

namespace InfrastructurePOCApp
{
    public class InfrastructureQuickTour : IEnableLogger
    {
        public void RunSQLiteQuickTour()
        {
            Locator.CurrentMutable.RegisterConstant(new SQLitePlatformWinRT(), typeof(ISQLitePlatform));
            Locator.CurrentMutable.RegisterConstant(new DebugLogger(), typeof(ILogger));

            ISQLiteAccessService service = new SQLiteAccessService();

            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path.ToString(), "fooBar.db");
            var database = service.GetDatabase(path);

            using (var conn = database.GetReadWriteConnection())
            {
                conn.CreateTable<Foo>();
                conn.CreateTable<Bar>();

                var bars = new List<Bar>
                {
                    new Bar { BarBar = "bar bar 1!" },
                    new Bar { BarBar = "bar bar 2!" }
                };

                var foo = new Foo { FooFoo = "foo foo!", Bars = bars };

                conn.InsertWithChildren(foo);

                var theWholeEnchilada = conn.GetAllWithChildren<Foo>().First();

                this.Log().Debug("Foo: ");
                this.Log().Debug("FooFoo: " + theWholeEnchilada.FooFoo);
                this.Log().Debug("Foo's Bars: ");
                theWholeEnchilada.Bars.ForEach(bar => this.Log().Debug("BarBar: " + bar.BarBar));

                this.Log().Debug("Cascading inserts and reads son!");
            }

            var sw = new Stopwatch();

            this.Log().Debug("Attempting to get a read-write conn and wait 3 secs in a parallel loop 5 times.");
            this.Log().Debug("Since only one read-write conn be in use at a time this should take about 15 secs.");

            sw.Start();

            Parallel.For(0, 5, i =>
            {
                using (var conn = database.GetReadWriteConnection())
                {
                    Task.Delay(3000).Wait();
                }
            });

            sw.Stop();

            this.Log().Debug("It took {0} secs.", sw.Elapsed.Seconds);

            sw.Reset();

            this.Log().Debug("Attempting to get a read-only conn and wait 3 secs in a parallel loop 5 times.");
            this.Log().Debug("Multiple read-only conns can be in use at a time so this should only take about 3 secs.");

            sw.Start();

            Parallel.For(0, 5, i =>
            {
                using (var conn = database.GetReadOnlyConnection())
                {
                    Task.Delay(3000).Wait();
                }
            });

            sw.Stop();

            this.Log().Debug("It took {0} secs.", sw.Elapsed.Seconds);

            service.CloseAllDatabases();
        }

        #region DTO's

        class Foo
        {
            [PrimaryKey, AutoIncrement]
            public int FooId { get; set; }

            public string FooFoo { get; set; }

            [OneToMany(CascadeOperations = CascadeOperation.All)]
            public List<Bar> Bars { get; set; }
        }

        class Bar
        {
            [PrimaryKey, AutoIncrement]
            public int BarId { get; set; }

            [ForeignKey(typeof(Foo))]
            public int FooId { get; set; }

            public string BarBar { get; set; }

            [ManyToOne]
            public Foo Foo { get; set; }
        }

        #endregion
    }
}
