﻿//// Proof of concept
using System.Threading.Tasks;

namespace ReactiveUIDemo.Search.Services
{
    public class SearchService : ISearchService
    {
        public const string Prefix = "Search results for ";

        /// <summary>
        /// Meant to feign a latent search service that takes 2 seconds to return a result.
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns>a string</returns>
        public async Task<string> GetSearchResultsAsync(string queryString)
        {
             await Task.Delay(2000).ConfigureAwait(false);

             return Prefix + queryString;
        }
    }
}
