﻿// Proof of concept
using System.Threading.Tasks;

namespace ReactiveUIDemo.Search.Services
{
    public interface ISearchService
    {
        /// <summary>
        /// Return search result string asynchronously.
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns>
        /// The search results
        /// </returns>
        Task<string> GetSearchResultsAsync(string queryString);
    }
}
