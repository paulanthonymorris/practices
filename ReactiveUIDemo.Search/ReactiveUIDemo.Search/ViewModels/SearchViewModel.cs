﻿// Proof of concept
using ReactiveUI;
using ReactiveUIDemo.Search.Services;
using Splat;
using System;
using System.Reactive.Linq;

namespace Search.ViewModels
{
    public class SearchViewModel : ReactiveObject
    {
        private string _queryText;
        public string QueryText
        {
            get { return _queryText; }
            set { this.RaiseAndSetIfChanged(ref _queryText, value); }
        }

        private string _resultText;
        public string ResultText
        {
            get { return _resultText; }
            set { this.RaiseAndSetIfChanged(ref _resultText, value); }
        }

        private readonly IReactiveCommand<string> _callSearchService;

        /// <summary>
        /// Setup the lookup logic, and use the interface to do the search service call.
        /// </summary>
        /// <param name="service"></param>
        public SearchViewModel(ISearchService service = null)
        {
            // So what's with the null default and using the service location feature inside the constructor?
            // This allows us to pass in a mock implementation of the interface for testing. If we pass nothing 
            // in then it falls back to what we registered in the locator at runtime.
            service = service ?? Locator.Current.GetService<ISearchService>();

            // Only allow a call to the search service when the query text is not empty.
            var newSearchNeeded = this.WhenAny(vm => vm.QueryText, queryText => !string.IsNullOrWhiteSpace(queryText.Value));

                                                                 // Tells the command whether it can execute.
            _callSearchService = ReactiveCommand.CreateAsyncTask(newSearchNeeded,
                // The Task to execute.
                async _ => await service.GetSearchResultsAsync(QueryText).ConfigureAwait(false),
                // We do this work off the UI thread.
                RxApp.TaskpoolScheduler); 

            // Subscribes to the result string and takes care of marshalling back to the UI.
            _callSearchService.Subscribe(result => ResultText = result);

            _callSearchService.ThrownExceptions
                .Subscribe(ex => UserError.Throw("Error connecting to search service", ex));

                // Whenever this value changes...
            this.WhenAnyValue(vm => vm.QueryText)
                // Throttling to delay one sec since the last update, resetting on each update...
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler) 
                // Then call the service asynchronously.
                .InvokeCommand(this, vm => vm._callSearchService);
        }
    }
}
