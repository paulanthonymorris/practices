﻿// Proof of concept
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReactiveUIDemo.Search.Services;
using Splat;
using System.Threading.Tasks;

namespace ReactiveUIDemo.SearchTest.Services
{
    [TestClass]
    public class SearchServiceTest
    {
        [ClassInitialize]
        public static void InitializeClass(TestContext ctx)
        {
            Locator.CurrentMutable.RegisterConstant(new SearchService(), typeof(ISearchService));
        }

        [TestMethod]
        public async Task TestGetSearchResultsAsync()
        {
            const string query = "test query";
            var service = Locator.Current.GetService<ISearchService>();

            Assert.AreEqual(SearchService.Prefix + query, await service.GetSearchResultsAsync(query));
        }
    }
}
