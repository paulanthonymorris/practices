﻿// Proof of concept
using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ReactiveUI.Testing;
using ReactiveUIDemo.Search.Services;
using Search.ViewModels;
using System.Threading.Tasks;

namespace ReactiveUIDemo.SearchTest.ViewModels
{
    [TestClass]
    public class SearchViewModelTest
    {
        private static ISearchService _zeroLatencyMockService;

        private const string myQuery = "my query";

        /// <summary>
        /// Read the attribute.
        /// </summary>
        [ClassInitialize]
        public static void InitializeClass(TestContext ctx)
        {
            var mock = new Mock<ISearchService>();
            mock.Setup(svc => svc.GetSearchResultsAsync(It.IsAny<string>()))
                .Returns((string query) => Task.FromResult(SearchService.Prefix + query));

            _zeroLatencyMockService = mock.Object;
        }

        /// <summary>
        /// Read the method name.
        /// </summary>
        [TestMethod]
        public void TestCannotExecuteSearchCallCommandIfQueryTextIsEmpty()
        {
            new TestScheduler().With(sched =>
            {
                var vm = new SearchViewModel(_zeroLatencyMockService);
                vm.QueryText = string.Empty;

                sched.AdvanceToMs(1001);
                // We should not have called the service.
                Assert.AreEqual(null, vm.ResultText);
            });
        }

        /// <summary>
        /// Read the method name.
        /// </summary>
        [TestMethod]
        public void TestSearchIsNotCalledUntilAfterOneSecond()
        {           
            // TestScheduler allows for on-demand time travel through UI scenarios.
            new TestScheduler().With(sched =>
            {
                var vm = new SearchViewModel(_zeroLatencyMockService);
                vm.QueryText = myQuery;

                // Run the scheduler forward to 999 ms. At that point, we should not have called the service yet.
                sched.AdvanceToMs(999);
                Assert.AreEqual(null, vm.ResultText);

                // Run the scheduler 1 ms past a second and the result should show up.
                sched.AdvanceToMs(1001);
                Assert.AreEqual(SearchService.Prefix + myQuery, vm.ResultText);
            });
        }

        /// <summary>
        /// Tests the scenario of user types something, pauses, then types something again.
        /// </summary>
        [TestMethod]
        public void TestThrottlingServiceCallWhileQueryIsStillBeingUpdated()
        {
            new TestScheduler().With(sched =>
            {
                var vm = new SearchViewModel(_zeroLatencyMockService);
                vm.QueryText = "my ";

                // Run the scheduler forward 300 ms, and change the input. mocking the user typing again after a brief pause.
                sched.AdvanceToMs(300);
                vm.QueryText = myQuery;

                // Now, at 1001 since the first value, the search service should not have been called yet.
                sched.AdvanceToMs(1001);
                Assert.AreEqual(null, vm.ResultText);

                // Now, after 300 *more* ms (one second after the second input event), the result should finally appear.
                sched.AdvanceByMs(300);
                Assert.AreEqual(SearchService.Prefix + myQuery, vm.ResultText);
            });
        }
    }
}
